@extends('vueadmin::layouts.app')

@section('content')
    @component('vueadmin::components.full-page-section')

        @component('vueadmin::components.card')
            @slot('title')
                <span class="icon"><i class="mdi mdi-lock"></i></span>
                <span>Авторизация</span>
            @endslot
            <form method="POST" action="{{ route('vueadmin.login') }}" name="login" onsubmit="sendForm(event)">
                @csrf

                <div class="field">
                    <div class="notification is-danger is-hidden" id="errors">

                    </div>
                    <label class="label" for="email">Email</label>
                    <div class="control">
                        <input id="email" type="email" class="input" name="email" value="{{ old('email') }}" required
                               autocomplete="email" autofocus>
                    </div>

                </div>

                <div class="field">
                    <label class="label" for="password">Пароль</label>
                    <div class="control">
                        <input id="password" type="password" class="input" name="password" required
                               autocomplete="current-password" autofocus>
                    </div>
                </div>

                <div class="control">
                    <label tabindex="0" class="b-checkbox checkbox is-thin">
                        <input type="checkbox" value="false" name="remember"
                               id="remember" {{ old('remember') ? 'checked' : '' }}>
                        <span class="check is-black"></span>
                        <span class="control-label">Запомнить</span>
                    </label>
                </div>

                <hr>

                <div class="field is-form-action-buttons">
                    <button type="submit" class="button is-black">
                        Вход
                    </button>
                </div>
            </form>

            <script>
                function sendForm(e) {
                    e.preventDefault();
                    var errors = document.getElementById('errors')
                    errors.classList.add('is-hidden')
                    var formData = new FormData(document.forms.login);
                    // отослать
                    var xhr = new XMLHttpRequest();
                    xhr.open("POST", e.target.action, false);
                    xhr.send(formData);
                    if (xhr.status == 422) {
                        errors.innerHTML = 'Не верный Email или пароль.'
                        errors.classList.remove('is-hidden')
                    }
                    if (xhr.status == 200) {
                        window.location = xhr.responseURL;
                    }
                }
            </script>
        @endcomponent
    @endcomponent
@endsection
