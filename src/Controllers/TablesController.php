<?php

namespace Cdonut\VueAdmin\Controllers;

use Cdonut\VueAdmin\Exceptions\VueAdminException;
use Cdonut\VueAdmin\Resources\AdminCollection;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Str;

class TablesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index(Request $request, $model)
    {
        $class = $this->getModelClass($model);
        $query = $class::query();
        if ($request->has('sort_by')) {
            $orderBy = explode('.', $request->sort_by);
            if ($orderBy[0] == 'created' || $orderBy[0] == 'updated')
                $orderBy[0] = $orderBy[0] . '_at';

            $query->orderBy(...$orderBy);
        }

        if ($request->has('filter')) {
             $class::applyFilter($request->filter, $query);
        }

        return new AdminCollection($query->paginate($request->perPage), $class);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param $model
     * @return \Illuminate\Http\Response
     * @throws VueAdminException
     */
    public function create($model)
    {
        $class = $this->getModelClass($model);
        if (!$class::canEdit()) {
            throw new VueAdminException("Недостаточно прав", 403);
        }
        $model = $this->getModel($model);
        return $model->getAdminForm();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $model
     * @return \Illuminate\Http\Response
     * @throws VueAdminException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request, $model)
    {

        $class = $this->getModelClass($model);
        if (!$class::canEdit()) {
            throw new VueAdminException("Недостаточно прав", 403);
        }
        $maxRecords = $class::adminMaxRecords();
        if($maxRecords!==null && ($class::count()>=$maxRecords)){
            throw new VueAdminException("Максимальное количество записей: $maxRecords", 400);
        }
        \Validator::make($request->all(), $class::getValidation(), $class::getValidationMessages())->validate();
        $item = new $class($request->all());
        $item->saveAdmin();
        $item->refresh();

        return $item->id;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $model
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws VueAdminException
     */
    public function edit($model, $id)
    {
        $class = $this->getModelClass($model);
        if (!$class::canView()) {
            throw new VueAdminException("Недостаточно прав", 403);
        }
        return $this->getModel($model, $id)->getAdminForm();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $model
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws VueAdminException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $model, $id)
    {
        $class = $this->getModelClass($model);
        if (!$class::canEdit()) {
            throw new VueAdminException("Недостаточно прав", 403);
        }
        $item = $class::find($id);
        \Validator::make($request->all(), $class::getValidation(), $class::getValidationMessages())->validate();
        $item->fill($request->all());

        if (!empty($request->moderate)) {
            if ($request->moderate == 'reject')
                $item->moderationReject();
            elseif ($request->moderate == 'confirm')
                $item->moderationConfirm();
        } else {
            $item->saveAdmin();
        }

        return $item->refresh()->getAdminForm();

    }

    public function getSingle(Request $request, $model)
    {
        $class = $this->getModelClass($model);
        $model = $class::first();
        if(empty($model)){
            $model = new $class();
        }
        return $model->getAdminForm();


    }

    public function customAction(Request $request, $model, $id, $action)
    {
        $class = $this->getModelClass($model);

        $item = $class::find($id);

        if (empty($item))
            throw new VueAdminException("Item not found", 404);

        $actions = $item->getCustomActions();
        if (empty($actions))
            throw new VueAdminException("Custom action is empty", 404);

        $actionKey = array_search($action, array_column($actions, 'link'));

        if ($actionKey === false)
            throw new VueAdminException("Custom action not found", 404);

        $method = $actions[$actionKey]['method'];

        if (!method_exists($item, $method))
            throw new VueAdminException("Method not found", 404);

        if (!empty($actions[$actionKey]['fill_model']) && $actions[$actionKey]['fill_model'] == true) {
            \Validator::make($request->all(), $class::getValidation(), $class::getValidationMessages())->validate();
            $item->fill($request->all());
        }

        return $item->$method();
    }

    public function fileUpload(Request $request, $model)
    {
        $name = $request->file('file')->getClientOriginalName();
        $folder = strtolower($model) . '/' . date('ymdHis');
        $path = \Storage::disk(config('vueadmin.disk', 'public'))->putFileAs($folder, $request->file('file'), $name);
        $href = \Storage::disk(config('vueadmin.disk', 'public'))->url($path);
        return compact('path', 'href');
    }

    public function protectedFileUpload(Request $request, $model)
    {
        $name = $request->file('file')->getClientOriginalName();
        $userId = \Auth::guard('vueadmin')->user()->id;
        $folder = 'protected_files/admin/' . strtolower($model) . '/' . date('ymdHis');
        $path = \Storage::disk('local')->putFileAs($folder, $request->file('file'), $name);
        $href = \Storage::disk('local')->url($path);
        return compact('path', 'href');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $model
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws VueAdminException
     */
    public function destroy($model, $id)
    {
        $class = $this->getModelClass($model);
        if (!$class::canEdit()) {
            throw new VueAdminException("Недостаточно прав", 403);
        }
        return $class::destroy($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param $model
     * @return \Illuminate\Http\Response
     * @throws VueAdminException
     */
    public function massDestroy(Request $request, $model)
    {
        $class = $this->getModelClass($model);
        if (!$class::canEdit()) {
            throw new VueAdminException("Недостаточно прав", 403);
        }
        $ids = $request->ids;
        return $class::destroy($ids);
    }

    protected function getModel($modelName, $id = null)
    {
        $modelClass = $this->getModelClass($modelName);

        if ($id)
            return $modelClass::find($id);

        return new $modelClass();
    }

    protected function getModelClass($modelName)
    {
        $modelClass = '\\App\\' . Str::studly($modelName);

        if (!class_exists($modelClass))
            throw new VueAdminException("Class $modelClass not found", 404);

        if (!$modelClass::canView()) {
            throw new VueAdminException("Недостаточно прав", 403);
        }

        return $modelClass;
    }

    public function __construct()
    {
        $this->middleware(\Cdonut\VueAdmin\Middleware\Auth::class);
    }

    public function excelTemplate(Request $request, $model){

        $modelClass = $this->getModelClass($model);
        return $modelClass::excelTemplate();
    }

    public function excelExport(Request $request, $model){

        $modelClass = $this->getModelClass($model);
        return $modelClass::exportExcel($request->get('filter'));
    }

    public function excelImport(Request $request, $model){

        $modelClass = $this->getModelClass($model);
        $file = $request->file('file');
        return $modelClass::importExcel($file);
    }
}
