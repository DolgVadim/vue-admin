<?php

namespace Cdonut\VueAdmin\Controllers;


use App\AdminUser;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AdminController extends Controller
{

    use AuthenticatesUsers;

    protected function guard()
    {
        return \Auth::guard('vueadmin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('vueadmin::home');
    }

    public function config()
    {
        $user = \Auth::guard('vueadmin')->user();
        $menu = $this->getMenu($user);
        return [
            'admin_title' => config('vueadmin.admin_title', 'VueAdmin'),
            'yandex_geo_key' => config('vueadmin.yandex_geo_key'),
            'menu' => $menu,
            'user' => $user
        ];
    }

    protected function getMenu($user)
    {
        $menu = config('vueadmin.menu', []);

        $filteredMenu = [];
        foreach ($menu as $section) {
            $menuItems = [];
            foreach ($section['items'] as $key => $item) {
                if (isset($item['model'])) {
                    $modelClass = '\\App\\' . Str::studly($item['model']);
                    if (!class_exists($modelClass))
                        continue;

                    if ($modelClass::canView())
                        $menuItems[$key] = $item;

                } else {
                    $menuItems[$key] = $item;
                }
            }

            if (!empty($menuItems)) {

                $filteredMenu[] = [
                    'name' => $section['name'],
                    'opened' => $section['opened'] ?? true,
                    'items' => array_values($menuItems)];
            }

        }

        return $filteredMenu;
    }

//    protected $redirectTo = '/admin';

    protected function authenticated(Request $request, $user)
    {
        return redirect()->intended('/admin');
    }


    public function getRobots()
    {
        if (file_exists('robots.txt'))
            return file_get_contents('robots.txt');
        return '';
    }

    public function storeRobots(Request $request)
    {
        $res = file_put_contents('robots.txt', $request->text);
        if ($res !== false)
            return response(['message' => ['success' => true, 'text' => 'Файл robots.txt сохранен']], 200);

        return response(['message' => ['success' => false, 'text' => 'Файл robots.txt не сохранен']], 500);
    }

    public function getSiteMap()
    {
        if (file_exists('sitemap.xml'))
            return file_get_contents('sitemap.xml');
        return '';
    }

    public function storeSiteMap(Request $request)
    {
        $res = file_put_contents('sitemap.xml', $request->text);
        if ($res !== false)
            return response(['message' => ['success' => true, 'text' => 'Файл sitemap.xml сохранен']], 200);

        return response(['message' => ['success' => false, 'text' => 'Файл sitemap.xml не сохранен']], 500);
    }

    public function tinyUpload(Request $request)
    {
        $name = $request->file('file')->getClientOriginalName();
        $folder = 'mce';
        $path = \Storage::disk(config('vueadmin.disk', 'public'))->putFileAs($folder, $request->file('file'), $name);
        $href = \Storage::disk(config('vueadmin.disk', 'public'))->url($path);
        return ['location' => $href];
    }
}
