<?php

namespace Cdonut\VueAdmin\Controllers;

use App\Http\Controllers\Controller;

class ProtectedFilesController extends Controller
{
    public function getFile($id, $path)
    {
        $allowed = false;
        if ($user = \Auth::user())
            $allowed = $user->id == $id;

        if (\Auth::guard('vueadmin')->check())
            $allowed = true;

        if (!$allowed)
            return response('not authorized', 401);

        $path = storage_path('app/protected_files/' . $id . '/' . $path);

        if (file_exists($path)) {
            return response()->file($path);
        } else {
            return response('not found', 404);
        }
    }
}