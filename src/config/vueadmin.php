<?php

return [
    'menu' =>
        [
            [
                'name' => 'Общие',
                'items' => [
                    [
                        'to' => '/',
                        'icon' => 'desktop-mac',
                        'label' => 'Рабочий стол',
                    ],

                    [
                        'to' => '/users',
                        'label' => 'Пользователи',
                        'icon' => 'chevron-right',
                        'singleTitle' => 'Пользователь',
                        'model' => 'User'
                    ],

                ],

            ]
        ],

    'disk' => 'public',
    'admin_title' => env('APP_NAME', 'Vue Admin'),
    'yandex_geo_key' => env('YANDEX_GEO_KEY', '')
];
