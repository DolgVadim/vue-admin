<?php

namespace App;

use Cdonut\VueAdmin\Traits\Adminable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;

/**
 * Class AdminUser
 * @package App
 * @Field(name=id, label=Id, type=text, editable=false, column=1, width=80, order=0)
 * @Field(name=created_at, label=Создан, type=datetime, editable=false, column=20, order=20)
 * @Field(name=updated_at, label=Изменен, type=datetime, editable=false, order=30)
 * @Field(name=name, label=Имя, required=true)
 * @Field(name=email, label=E-mail, type=email, required=true, column=2)
 * @Field(name=password, label=Пароль, type=password)
 * @Field(name=role, label=Роль, type=select, required=true, options=0:Супер администратор|1:Администратор|2:Модератор, default=2, column=5)
 */
class AdminUser extends Authenticatable
{
    use Adminable;
    protected $fillable = ['name','email', 'password', 'role'];

    static function canView()
    {
        $user = \Auth::guard('vueadmin')->user();
        return isset($user->role) && $user->role == 0;
    }

    public static function getValidation()
    {
        return [
            'name' => 'required|string',
            'email' => 'required|email',
            'password' => 'sometimes|required|string',
            'role' => 'required|integer',
        ];
    }

    public static function getValidationMessages()
    {
        return [
            'name.required' => 'Заполните имя',
            'email.required' => 'Заполните email',
            'password.required' => 'Заполните пароль',
            'role.required' => 'Заполните роль',
        ];
    }

    protected function setPasswordAttribute($value)
    {
        $this->attributes['password'] = \Hash::make((string)$value);
    }

}
