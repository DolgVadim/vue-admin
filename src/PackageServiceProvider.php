<?php


namespace Cdonut\VueAdmin;

use Illuminate\Support\Carbon;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\Arr;
use Illuminate\Support\ServiceProvider;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Foundation\Console\PresetCommand;

class PackageServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/config/vueadmin.php', 'vueadmin'
        );
    }

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

        $this->publishes([__DIR__ . '/config/vueadmin.php' => config_path('vueadmin.php')], 'config');
        $this->publishes([__DIR__ . '/public' => public_path('packages/cdonut/vueadmin')], 'public');
        $this->publishes([__DIR__ . '/migrations' => database_path('migrations')], 'models');
        $this->publishes([__DIR__ . '/Models' => app_path()], 'models');
        $this->loadViewsFrom(__DIR__ . '/views', 'vueadmin');
        $this->loadRoutesFrom(__DIR__ . '/routes.php');
        $this->app['router']->aliasMiddleware('role', \Cdonut\VueAdmin\Middleware\Role::class);
        $this->app['router']->aliasMiddleware('adminauth', \Cdonut\VueAdmin\Middleware\Auth::class);
        $request = $this->app->request;
        if (strpos( $request->pathInfo,'/admin') === 0) {
            Carbon::serializeUsing(function ($carbon) {
                return $carbon->format('Y-m-d H:i:s');
            });
        }

    }
}
