<?php

namespace Cdonut\VueAdmin\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class AdminCollection extends ResourceCollection
{
    protected $modelClass;

    /**
     * Create a new resource instance.
     *
     * @param  mixed $resource
     * @return void
     */
    public function __construct($resource, $modelClass)
    {
        parent::__construct($resource);

        $this->modelClass = $modelClass;
    }

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }

    public function with($request)
    {

        $disableCreation = false;
        $maxRecords = $this->modelClass::adminMaxRecords();
        if($maxRecords!==null && ($maxRecords<=$this->modelClass::count()))
        {
            $disableCreation = "Максимальное количество записей: $maxRecords";
        }
        return [
            'columns' => $this->modelClass::getColumns(),
            'filters' => $this->modelClass::applyFilter($request->filter),
            'disableCreation'=>$disableCreation,
            'excelImportEnabled'=>$this->modelClass::excelImportEnabled(),
            'excelExportEnabled'=>$this->modelClass::excelExportEnabled(),
        ];
    }
}
