import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {

        /* NavBar */
        isNavBarVisible: true,

        /* FooterBar */
        isFooterBarVisible: true,

        /* Aside */
        isAsideVisible: true,
        isAsideMobileExpanded: false,
        menu: [],
        user: [],
        models: [],
        filters: {},
        admin_title: '',
        yandex_geo_key: ''
    },
    mutations: {
        /* A fit-them-all commit */
        basic(state, payload) {
            state[payload.key] = payload.value
        },

        menu(state, payload) {
            state.menu = payload
        },

        filters(state, payload) {
            if (payload === 'remove')
                delete state.filters[payload.model];
            else {
                if (!state.filters[payload.model])
                    state.filters[payload.model] = {}
                if (payload.hasOwnProperty('opened'))
                    state.filters[payload.model].opened = payload.opened
                if (payload.hasOwnProperty('active'))
                    state.filters[payload.model].active = payload.active
                if (payload.hasOwnProperty('filters')) {
                    if (payload.filters === false)
                        delete state.filters[payload.model].filters
                    else
                        state.filters[payload.model].filters = payload.filters
                }
            }

        },

        user(state, payload) {
            state.user = payload
        },

        admin_title(state, payload) {
            state.admin_title = payload
        },

        yandex_geo_key(state, payload) {
            state.yandex_geo_key = payload
        },

        models(state, payload) {
            state.models = payload
        },

        /* Aside Mobile */
        asideMobileStateToggle(state, payload = null) {
            const htmlClassName = 'has-aside-mobile-expanded'

            let isShow

            if (payload !== null) {
                isShow = payload
            } else {
                isShow = !state.isAsideMobileExpanded
            }

            if (isShow) {
                document.documentElement.classList.add(htmlClassName)
            } else {
                document.documentElement.classList.remove(htmlClassName)
            }

            state.isAsideMobileExpanded = isShow
        }
    },
    getters: {
        MENU: state => {
            return state.menu
        },
        FILTERS: state => {
            return state.filters
        },
        MODELS: state => {
            return state.models
        },
        USER: state => {
            return state.user
        },
        ADMIN_TITLE: state => {
            return state.admin_title
        },
        YANDEX_GEO_KEY: state => {
            return state.yandex_geo_key
        }
    },
    actions: {}
})
