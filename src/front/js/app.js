// Axios & Echo global
import {api} from '@/common'

require('./bootstrap');

/* Core */
import Vue from 'vue'
import Buefy from 'buefy'

/* Router & Store */
import router from './router'
import store from './store'

/* Vue. Main component */
import App from './App.vue'

/* Vue. Component in recursion */
import AsideMenuList from '@/components/AsideMenuList'

/* Collapse mobile aside menu on route change */
router.afterEach(() => {
    store.commit('asideMobileStateToggle', false)
})

Vue.config.productionTip = false

var VueCookie = require('vue-cookie');
// Tell Vue to use the plugin
Vue.use(VueCookie);

/* These components are used in recursion algorithm */
Vue.component('AsideMenuList', AsideMenuList)

/* Main component */
Vue.component('App', App)

/* Buefy */
Vue.use(Buefy)
// const moment = require('moment')


import VueMoment from 'vue-moment'
import moment from 'moment-timezone'

require('moment/locale/ru')
Vue.use(VueMoment, {
    moment,
})

let menuState = [];
if (document.cookie) {
    let matches = Array.from(document.cookie.matchAll(/menu\[(\d)\]\.opened=(true|false)/g));
    matches.forEach((match) => {
        menuState[match[1]] = match[2] == 'true'
    })
}

api.get('/config')
    .then(r => {
        let models = [];
        r.data.menu.forEach((menuGroup, index) => {
            if (menuState[index] !== undefined)
                r.data.menu[index].opened = menuState[index]
            else
                r.data.menu[index].opened = true
            menuGroup.items.forEach((menu) => {
                if (menu.model) {
                    let showListActions = true;
                    if ('showListActions' in menu && menu.showListActions == false)
                        showListActions = false;

                    let sTitle = menu.singleTitle? menu.singleTitle: menu.single_title

                    models.push({
                        path: menu.to,
                        model: menu.model,
                        sort: menu.sort,
                        order: menu.order,
                        showListActions: showListActions,
                        title: menu.label,
                        icon: menu.icon,
                        redirectOnSave: menu.redirectOnSave!==false,
                        singleTitle: sTitle
                    })
                }
            })
        })
        window.menu = r.data.menu
        window.models = models
        window.user = r.data.user
        window.admin_title = r.data.admin_title
        window.yandex_geo_key = r.data.yandex_geo_key
        /* This is main entry point */
        new Vue({
            store,
            router,
            render: h => h(App),
            mounted() {
                document.documentElement.classList.remove('has-spinner-active')
            }
        }).$mount('#app')

    })
    .catch(err => {
        console.log(err)
    })
