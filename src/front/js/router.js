import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import List from "./views/List";
import Detail from "./views/Detail";
import Robots from "./views/Robots";

Vue.use(Router)

export default new Router({
    base: 'admin',
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/robots',
            name: 'robots',
            component: Robots
        },
        {
            path: '/profile',
            name: 'profile',
            component: () => import(/* webpackChunkName: "profile" */ './views/Profile.vue')
        },
        {
            path: '/single/:slug',
            name: 'single',
            component: Detail,
            props: {singleModel:true}
        },
        {
            path: '/:slug',
            name: 'list',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: List,
            props: true
        },
        {
            path: '/:slug/:id',
            name: 'detail',
            component: Detail,
            props: true
        },

    ],
    scrollBehavior(to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition
        } else {
            return {x: 0, y: 0}
        }
    }
})
