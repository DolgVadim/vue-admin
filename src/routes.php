<?php

Route::middleware(['web'])->get('/storage/protected_files/{id}/{path}',
    '\Cdonut\VueAdmin\Controllers\ProtectedFilesController@getFile')->where('path', '.*');

Route::prefix('admin')->middleware(['web'])->group(function () {
    Route::group(["namespace" => "Cdonut\VueAdmin\Controllers"], function () {
        Route::view('login', 'vueadmin::auth.login');
        Route::post('login', 'AdminController@login')->name('vueadmin.login');
        Route::group(['middleware' => 'adminauth:vueadmin'], function () { //'middleware' => 'vueadminauth'
            Route::prefix('public_files')->group(function () {
                Route::get('/robots', 'AdminController@getRobots');
                Route::post('/robots', 'AdminController@storeRobots');
                Route::get('/sitemap', 'AdminController@getSiteMap');
                Route::post('/sitemap', 'AdminController@storeSiteMap');
                Route::post('/tinymce/upload', 'AdminController@tinyUpload');
            });
            Route::get('/', 'AdminController@index')->name('vueadmin.home');
            Route::get('/config', 'AdminController@config');
            Route::any('logout', 'AdminController@logout')->name('vueadmin.logout');
            Route::prefix('tables')->group(function () {
                Route::get('/single_model/{model}', 'TablesController@getSingle')->name('tables.single');
                Route::get('/{model}', 'TablesController@index')->name('tables.index');
                Route::post('/{model}', 'TablesController@store')->name('tables.store');
                Route::get('/{model}/excelExport', 'TablesController@excelExport')->name('tables.excel.export');
                Route::post('/{model}/excelImport', 'TablesController@excelImport')->name('tables.excel.import');
                Route::get('/{model}/excelTemplate', 'TablesController@excelTemplate')->name('tables.excel.template');
                Route::get('/{model}/create', 'TablesController@create')->name('tables.create');
                Route::post('/{model}/fileUpload', 'TablesController@fileUpload')->name('tables.upload');
                Route::post('/{model}/protectedFileUpload', 'TablesController@protectedFileUpload')->name('tables.protected.upload');
                Route::delete('/{model}/{id}', 'TablesController@destroy')->name('tables.destroy');
                Route::get('/{model}/{id}', 'TablesController@show')->name('tables.show');
                Route::post('/{model}/{id}', 'TablesController@update')->name('tables.update');
                Route::delete('/{model}', 'TablesController@massDestroy')->name('tables.mass.destroy');
                Route::get('/{model}/{id}/edit', 'TablesController@edit')->name('tables.edit');
                Route::any('/{model}/{id}/{action}', 'TablesController@customAction')->name('tables.customAction');
            });
            Route::fallback('AdminController@index');
        });
    });

});