<?php

namespace Cdonut\VueAdmin\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Role
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, ...$roles)
    {
        if (empty($roles))
            return $next($request);

        $user = Auth::user();

        foreach ($roles as $role) {
            // Check if user has the role This check will depend on how your roles are set up
            if (in_array($role, $user->roles))
                return $next($request);
        }

        return redirect('login');

    }
}