<?php

namespace Cdonut\VueAdmin\Traits;


use App\Exceptions\ApiException;
use Box\Spout\Common\Entity\Cell;
use Box\Spout\Common\Entity\Style\CellAlignment;
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
use Box\Spout\Writer\Common\Creator\Style\StyleBuilder;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Carbon\Carbon;
use Cdonut\VueAdmin\Exceptions\VueAdminException;
use Cdonut\VueAdmin\Middleware\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

trait Adminable
{
    static $reflector;
    static $enableMainActions = true;
    static $fields = [];
    static $fieldDefaults = [
        'type' => 'text',
        'required' => false,
        'sortable' => true,
        'order' => 100
    ];

    static function mainActionsIsEnabled()
    {
        return static::canView();
    }

    static function adminMaxRecords()
    {
        return null;
    }

    static function canEdit()
    {
        return static::canView();
    }

    static function canView()
    {
        $user = \Auth::guard('vueadmin')->user();
        return isset($user->role) && $user->role <= 1;
    }

    static function getValidation(): array
    {
        return [];
    }

    static function getValidationMessages(): array
    {
        return [];
    }

    static function getAdminCards()
    {
        return [];
    }

    public function getCustomActions()
    {
        $reflector = self::getReflector();
        $props = $reflector->getProperties();
        $customActions = [];

        foreach ($props as $prop) {
            if (strpos($prop->name, 'admin_customActions') !== false) {
                $prop->setAccessible(true);
                $customActions = array_merge($customActions, $prop->getValue($this));
            }
        }

        return $customActions;
    }

    static function getFilters()
    {
        return null;
    }

    static function applyFilter($data = null, $query = null)
    {

        $filters = self::getFilters();
        if ($data === null)
            return $filters;

        if (is_string($data))
            $data = json_decode($data, true);
        foreach ($filters as $key => $filter) {
            $dataIndex = array_search($filter['label'], array_column($data, 'label'));
            if ($dataIndex !== false)
                $filters[$key]['value'] = $data[$dataIndex]['value'];
        }
        if ($query === null)
            return $filters;

        foreach ($filters as $filter) {
            if (($filter['compare'] == 'like' && $filter['value'] === '') ||
                ($filter['compare'] == '=' && ($filter['value'] === '' || $filter['value'] === null)) ||
                ($filter['value'] === false && $filter['type'] != 'boolean')
            )
                continue;

            if ($filter['compare'] == 'like')
                $query->where($filter['field'], 'like', '%' . $filter['value'] . '%');
            elseif ($filter['compare'] == 'func') {
                if (!method_exists(self::class, $filter['func']))
                    throw new VueAdminException('filter func not found', 404);

                $query = self::{$filter['func']}($query, $filter['value']);
            } else
                $query->where($filter['field'], $filter['compare'], $filter['value']);
        }
    }

    static function getColumns()
    {
        $columns = array_filter(static::getFields(), function ($item) {
            return isset($item['column']);
        });
        usort($columns, function ($a, $b) {
            return $a['column'] <=> $b['column'];
        });
        return array_values($columns);
    }

    static function getFields(bool $editableOnly = false, $skipAdminCheck = false)
    {
        if (!empty(self::$fields)) return static::$fields;
        foreach (static::getDockProps('Field') as $key => $field) {
            $name = $key;
            $props = [];
            $settings = explode(',', $field);
            foreach ($settings as $settingKey => $setting) {
                if (strpos($setting, '=') === false) {
                    $searchIndex = $settingKey;
                    do {
                        $searchIndex--;
                        if (!isset($settings[$searchIndex]))
                            continue;

                        if (strpos($settings[$searchIndex], '=') !== false) break;
                    } while ($searchIndex >= 0);

                    if (!empty($settings[$searchIndex])) {
                        $settings[$searchIndex] .= ',' . $setting;
                        unset($settings[$settingKey]);
                    }
                }
            }
            $settings = array_values($settings);
            foreach ($settings as $setting) {
                if (empty($setting)) continue;
                $parts = explode('=', $setting);
                $propKey = trim($parts[0], " *\t\n\r\0\x0B");
                $propValue = trim($parts[1], " *\t\n\r\0\x0B");
                if ($propValue == "false" || $propValue == "true")
                    $propValue = $propValue == "true";

                if ($propKey == 'name')
                    $name = $propValue;
                $props[$propKey] = $propValue;
            }
            if (empty($props['name']))
                $props['name'] = $name;

            static::$fields[$name] = $props;

            static::$fields[$name] = array_merge(static::$fieldDefaults, static::$fields[$name]);
            if (static::$fields[$name]['type'] == 'relation' && !empty(static::$fields[$name]['relationModel'])) {
                $relationField = !empty(static::$fields[$name]['relationField']) ? static::$fields[$name]['relationField'] : 'name';
                $class = '\\App\\' . static::$fields[$name]['relationModel'];
                if (isset(static::$fields[$name]['data'])) {
                    $method = static::$fields[$name]['data'];
                    static::$fields[$name]['values'] = static::$method()->toArray();
                } else
                    static::$fields[$name]['values'] = $class::all(['id', $relationField . ' AS name'])->toArray();
            }
            if (static::$fields[$name]['type'] == 'file') {
                static::$fields[$name]['sortable'] = false;
            }

            if (static::$fields[$name]['type'] == 'select') {
                if (isset(static::$fields[$name]['data'])) {
                    $method = static::$fields[$name]['data'];
                    $selectValues = static::$method();
                    foreach ($selectValues as $key => $val) {
                        static::$fields[$name]['values'][] = ['id' => $key, 'value' => $val];
                    }
                } else {
                    static::$fields[$name]['values'] =
                        array_map(function ($val) {
                            $parts = array_map('trim', explode(':', $val));
                            return ['id' => $parts[0], 'value' => $parts[1]];
                        }, explode('|', static::$fields[$name]['options']));
                }
            }

            if (static::$fields[$name]['type'] == 'json') {
                $method = static::$fields[$name]['schema'];
                if (!empty($method))
                    static::$fields[$name]['schema'] = static::$method();
            }

            if (isset(static::$fields[$name]['editable'])) {

                if (!is_bool(static::$fields[$name]['editable'])) {
                    if ($skipAdminCheck)
                        $role = 0;
                    else {
                        $user = \Auth::guard('vueadmin')->user();
                        $role = $user->role;
                    }
                    static::$fields[$name]['editable'] = (int)static::$fields[$name]['editable'] >= $role;
                }
            } else
                static::$fields[$name]['editable'] = true;

            if (static::$fields[$name]['editable'] == false && $editableOnly) {
                unset(static::$fields[$name]);
            }
        }

        usort(static::$fields, function ($a, $b) {
            return $a['order'] <=> $b['order'];
        });

        return static::$fields;
    }

    public function getAdminForm()
    {
        $fields = self::getFields();
        foreach ($fields as &$field) {
            $fieldName = $field['name'];

            if ($field['type'] !== 'password')
                $field['value'] = $this->getAttribute($fieldName) ?? $field['default'] ?? null;

            $field['errors'] = null;
        }
        unset($field);
        $cards = [];
        if (!empty($cardsTemplate = static::getAdminCards())) {
            $fieldsInCards = [];
            foreach ($cardsTemplate as $template) {
                $fieldNames = $template['fields'];
                $fieldsInCards = array_merge($fieldsInCards, $fieldNames);
                $template['fields'] = array_values(
                    array_filter($fields, function ($item) use ($fieldNames) {
                        return in_array($item['name'], $fieldNames);
                    })
                );
                $cards[] = $template;
            }
            $ungroupedFields = array_values(
                array_filter($fields, function ($item) use ($fieldsInCards) {
                    return !in_array($item['name'], $fieldsInCards);
                })
            );
            if (!empty($ungroupedFields))
                $cards[] = [
                    'title' => 'Свойства',
                    'class' => 'is-12',
                    'icon' => 'ballot',
                    'fields' => array_values($ungroupedFields)
                ];
        } else {
            $cards = [
                [
                    'title' => 'Свойства',
                    'class' => 'is-12',
                    'icon' => 'ballot',
                    'fields' => array_values($fields)
                ],
            ];
        }

        $customActions = $this->getCustomActions();

        $enableMainActions = self::mainActionsIsEnabled();

        $id = $this->id ?: 'new';

        return compact('id', 'cards', 'customActions', 'enableMainActions');
    }

    static function getDockProps($keyword)
    {
        $rc = self::getReflector();
        $classProps = $rc->getProperties();
        $fields = [];

        foreach ($classProps as $prop) {
            $comment = $rc->getProperty($prop->name)->getDocComment();
            if (empty($comment)) continue;

            $formComment = [];
            preg_match('/@' . $keyword . '\((.*)\)\s/sU', $comment, $formComment);
            if (empty($formComment)) continue;

            $fields[$prop->name] = $formComment[1] ?: null;
        }

        $classDoc = $rc->getDocComment();
        $virtualProps = [];
        preg_match_all('/@' . $keyword . '\((.*)\)\s/sU', $classDoc, $virtualProps);
        if (!empty($virtualProps[1])) {
            foreach ($virtualProps[1] as $prop) {
                $fields[] = $prop;
            }
        }

        return $fields;
    }

    static function getReflector()
    {
        return self::$reflector ?: self::$reflector = new \ReflectionClass(self::class);
    }

    public function saveAdmin(array $options = [], bool $forceSave = false)
    {
        $fields = static::getFields(false, $forceSave);
        $relationFields = [];
        $protectedFiles = [];
        foreach ($fields as $field) {
            if ($field['editable'] != true && $forceSave == false) {
                $this[$field['name']] = $this->getOriginal($field['name']);
            }

            if ($field['type'] == 'relation' && (isset($field['relationMethod'])) || isset($field['saveMethod'])) {
                $field['value'] = $this->getAttribute($field['name']);
                unset($this[$field['name']]);
                $relationFields[] = $field;
            }

            if ($field['name'] == 'created_at' || $field['name'] == 'updated_at' || $field['name'] == 'deleted_at') {
                unset($this[$field['name']]);
            }

            if ($field['type'] == 'file' && isset($field['protected']) && $field['protected'] == 'true')
                $protectedFiles[] = $field;
        }

        parent::save($options);

        $resave = false;
        foreach ($protectedFiles as $file) {
            $oldPath = $this[$file['name']];
            if (strpos($oldPath, 'protected_files/admin/') !== false) {
                $getIdMethod = 'get' . $file['protectedId'];
                $protectedId = $this->$getIdMethod();
                $newPath = str_replace('protected_files/admin/', "protected_files/$protectedId/", $oldPath);
                if (\Storage::move($oldPath, $newPath)) {
                    $this->attributes[$file['name']] = $newPath;
                    $resave = true;
                }
            }
        }

        foreach ($relationFields as $field) {
            if (isset($field['saveMethod'])) {
                if ($field['saveMethod'] != "null")
                    static::$field['saveMethod']($field['value']);
            } elseif (isset($field['relationMethod'])) {
                $method = $field['relationMethod'];
                $this->$method()->sync($field['value']);
            }
        }

        if ($resave)
            parent::save($options);
    }

    static function getExcelExportFields()
    {
        $fields = static::getFields();
        $columns = [];
        foreach ($fields as $field) {
            $columns[] = ['label' => $field['label'], 'name' => $field['name']];
        }
        return $columns;
    }

    static function getExcelImportFields()
    {
        $fields = static::getFields(true);
        $columns = [];
        foreach ($fields as $field) {
            $columns[] = ['label' => $field['label'], 'name' => $field['name']];
        }
        return $columns;
    }

    public function excelImportBeforItemSave()
    {
        return $this;
    }

    public static function afterExcelImport()
    {

    }

    static function excelExportEnabled()
    {
        return false;
    }

    static function excelImportEnabled()
    {
        return false;
    }

    static function importExcel(\Illuminate\Http\UploadedFile $file)
    {
        $date = Carbon::now()->format('d_m_y_H_i');
        $dir = "protected_files/admin/excel_import/$date";
        $path = $file->storeAs($dir, $file->getClientOriginalName(), ['disk' => 'local']);
        $filePath = storage_path("app/$path");
        $reader = ReaderEntityFactory::createReaderFromFile($filePath);
        $reader->open($filePath);
        $columns = self::getExcelImportFields();
        $errors = [];
        foreach ($reader->getSheetIterator() as $sheet) {
            foreach ($sheet->getRowIterator() as $rowNum => $row) {
                if ($rowNum == 1) {
                    /**
                     * @var $row \Box\Spout\Common\Entity\Row
                     */
                    $cellsNum = $row->getNumCells();
                    if (count($columns) != $cellsNum)
                        return response()->json(['success' => false, 'message' => 'Количество колонок не совпадает с форматом импорта']);
                    continue;
                }
                $cells = $row->getCells();
                $fields = [];
                foreach ($columns as $index => $column) {
                    $fields[$column['name']] = $cells[$index]->getValue();
                }
                try {
                    $validation = self::getValidation();
                    $valid = true;
                    if (!empty($validation)) {
                        $validator = Validator::make($fields, $validation, self::getValidationMessages());
                        if ($validator->fails()) {
                            $message = implode(PHP_EOL, array_map(function ($element) {
                                return implode(PHP_EOL, $element);
                            }, $validator->getMessageBag()->getMessages()));

                            $errors[] = $row->addCell(new Cell($message));
                            $valid = false;
                        }
                    }
                    if ($valid) {
                        $modelObject = new self($fields);
                        $modelObject->excelImportBeforItemSave()->save();
                    }
                } catch (\Exception $e) {
                    $errors[] = $row->addCell(new Cell($e->getMessage()));
                }
            }
        }

        $reader->close();

        static::afterExcelImport();

        if (!empty($errors)) {
            $headerColumns = array_column($columns, 'label');
            $headerColumns[] = 'Ошибки';
            $header  = WriterEntityFactory::createRowFromArray($headerColumns, self::getHraderStyle());
            array_unshift($errors, $header);
            $errFilePath = storage_path("/app/$dir/errors.xlsx");
            $writer = WriterEntityFactory::createXLSXWriter();
            $writer->openToFile($errFilePath);
            $writer->addRows($errors);
            $writer->close();
            $url = url("/storage/$dir/errors.xlsx");
            return response()->json(['success' => false, 'message' => 'Не все записи были добавлены. <a download="errors.xlsx" href="' . $url . '">Скачать</a> файл с ошибкам.']);
        }

        return response()->json(['success' => true, 'message' => 'Все записи добавлены']);
    }

    static function excelTemplate()
    {
        $columns = self::getExcelImportFields();
        $rows = [];
        $rc = self::getReflector();
        $shortName = $rc->getShortName();
        $storageDir = 'protected_files/admin/excel_templates';
        $path = "$storageDir/$shortName.xlsx";

        $rows[] = WriterEntityFactory::createRowFromArray(array_column($columns, 'label'), self::getHraderStyle());
        $writer = WriterEntityFactory::createXLSXWriter();
        \Storage::makeDirectory($storageDir);
        $fileName = storage_path("/app/$path");
        $writer->openToFile($fileName);
        $writer->addRows($rows);

        $writer->close();

        return ['vueadmin_downloadfile' => url("/storage/$path")];
    }


    static function exportExcel($filter = null)
    {
        $query = self::query();
        if (!empty($filter)) {
            self::applyFilter($filter, $query);
        }

        $models = $query->get();
        $columns = self::getExcelExportFields();
        $rows = [];

        $rows[] = WriterEntityFactory::createRowFromArray(array_column($columns, 'label'), self::getHraderStyle());
        $writer = WriterEntityFactory::createXLSXWriter();
        $rc = self::getReflector();
        $shortName = $rc->getShortName();
        $storageDir = 'protected_files/admin/excel_export';
        $date = Carbon::now()->format('d_m_y_H_i');
        $path = "$storageDir/{$shortName}_{$date}.xlsx";
        Storage::makeDirectory($storageDir);
        $fileName = storage_path("/app/$path");
        $writer->openToFile($fileName);

        $models->each(function ($model) use (&$rows, $columns) {
            $row = [];
            foreach ($columns as $column) {
                $field = $column['name'];
                $row[] = (string)$model->$field;
            }
            $rows[] = WriterEntityFactory::createRowFromArray($row);
        });

        $writer->addRows($rows);

        $writer->close();

        return ['vueadmin_downloadfile' => url("/storage/$path")];
    }

    private static function getHraderStyle()
    {
        return (new StyleBuilder())
            ->setFontBold()
            ->setCellAlignment(CellAlignment::CENTER)
            ->build();
    }
}
